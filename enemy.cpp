#include "enemy.hpp"
#include "texturemanager.hpp"
#include "constants.hpp"
#include "game.hpp"
#include <iostream>

Enemy::Enemy(const char *texturesheet, int x, int y, int sizew, int sizeh)
{
    objTexture = TextureManager::LoadTexture(texturesheet);
    xpos = x;
    ypos = y;
    sourceRect.h = sizeh;
    sourceRect.w = sizew;
    notEnemy = false;
}

Enemy::Enemy() {}
Enemy::~Enemy() {}

void Enemy::update()
{

    if (!(xvel == 0))
    {
        xpos += xvel;
    }

    if (!(yvel == 0))
    {
        ypos += yvel;
    }

    
    sourceRect.x = 0;
    sourceRect.y = 0;

    destinationRect.x = xpos;
    destinationRect.y = ypos;
    destinationRect.w = sourceRect.w;
    destinationRect.h = sourceRect.h;

    collisionRect.x = xpos;
    collisionRect.y = ypos;
    collisionRect.w = 32;
    collisionRect.h = 32;

    if (xpos<0)
    {
       notRemovable = false;
    }
    if (ypos<STATUSBARHEIGHT || ypos>SCREENHEIGHT)
    {
       notRemovable = false;
    }
    
}

void Enemy::render()
{
    SDL_RenderCopy(Game::renderer, objTexture, &sourceRect, &destinationRect);
}