#include "game.hpp"
#include "gameobject.hpp"
#include "iostream"
#include <vector>
#include <list>
#include "player.hpp"
#include "constants.hpp"
#include "enemy.hpp"
#include <stdlib.h>

SDL_Renderer *Game::renderer = nullptr;
TTF_Font *Game::gFont = nullptr;

std::list<GameObject *> objects;

Player *player = nullptr;

GameObject *statusbar = nullptr;
GameObject *heart1, *heart2, *heart3 = nullptr;
Background *background1, *background2, *background3 = nullptr;
Background *backgroundB1, *backgroundB2, *backgroundB3 = nullptr;
GameText *scoreBar = nullptr;
GameText *gameOverScreen = nullptr;

const char *playerImage = "assets/player.png";
const char *enemyImage = "assets/enemy-sheet.png";
const char *backgroundImage = "assets/sky1.png";
const char *backgroundImage2 = "assets/sky2.png";
const char *statusbarImage = "assets/statusbar.png";
const char *heartImage = "assets/heart.png";
const char *playerBulletBasicImage = "assets/player_bullet2.png";

extern int Game::score = 0;
Game::Game()
{
}

Game::~Game()
{
}

void Game::init(const char *title, int xpos, int ypos, int width, int HEIGHT, bool fullscreen)
{
    int flags = 0;
    if (fullscreen)
    {
        flags = SDL_WINDOW_FULLSCREEN;
    }
    if (SDL_Init(SDL_INIT_EVERYTHING) == 0)
    {
        TTF_Init();
        gFont = TTF_OpenFont("font.ttf", 32);
        std::cout << "Subsystems initialised \n";
        window = SDL_CreateWindow(title, xpos, ypos, width, HEIGHT, flags);
        if (window)
        {
            std::cout << "Window created\n";
        }
        renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC);
        if (renderer)
        {
            SDL_SetRenderDrawColor(renderer, 0, 0, 0, 255);
            // SDL_RenderSetScale(renderer, 2,2);
            std::cout << "Renderer created\n";
        }
        isRunning = true;
    }
    else
    {
        isRunning = false;
    }

    player = new Player(playerImage, 80, 280);
    scoreBar = new GameText(300, 4);
    gameOverScreen = new GameText(200, SCREENHEIGHT / 2);

    statusbar = new GameObject(statusbarImage, 0, 0, SCREENWIDTH, STATUSBARHEIGHT);
    background1 = new Background(backgroundImage, 0, STATUSBARHEIGHT, SCREENWIDTH, SCREENHEIGHT, 1);
    background2 = new Background(backgroundImage, SCREENWIDTH, STATUSBARHEIGHT, SCREENWIDTH, SCREENHEIGHT, 1);
    background3 = new Background(backgroundImage, SCREENWIDTH * 2, STATUSBARHEIGHT, SCREENWIDTH, SCREENHEIGHT, 1);

    backgroundB1 = new Background(backgroundImage2, 0, STATUSBARHEIGHT, SCREENWIDTH, SCREENHEIGHT, 3);
    backgroundB2 = new Background(backgroundImage2, SCREENWIDTH, STATUSBARHEIGHT, SCREENWIDTH, SCREENHEIGHT, 3);
    backgroundB3 = new Background(backgroundImage2, SCREENWIDTH * 2, STATUSBARHEIGHT, SCREENWIDTH, SCREENHEIGHT, 3);

    heart1 = new GameObject(heartImage, 10, 4, 32, 32);
    heart2 = new GameObject(heartImage, heart1->xpos + heart1->sourceRect.w, heart1->ypos, heart1->sourceRect.w, heart1->sourceRect.h);
    heart3 = new GameObject(heartImage, heart2->xpos + heart2->sourceRect.w, heart2->ypos, heart2->sourceRect.w, heart2->sourceRect.h);

    objects.emplace_back(background1);
    objects.emplace_back(background2);
    objects.emplace_back(background3);
    objects.emplace_back(backgroundB1);
    objects.emplace_back(backgroundB2);
    objects.emplace_back(backgroundB3);
    objects.emplace_back(statusbar);
    objects.emplace_back(heart1);
    objects.emplace_back(heart2);
    objects.emplace_back(heart3);

    objects.emplace_back(scoreBar);

    objects.emplace_back(player);
}

void Game::handleEvents()
{
    SDL_Event event;

    while (SDL_PollEvent(&event))
    {
        switch (event.type)
        {
        case SDL_KEYDOWN:
            switch (event.key.keysym.sym)
            {
            case SDLK_q:
                isRunning = false;
                break;
            case SDLK_UP:
                player->yvel = -8;
                break;
            case SDLK_DOWN:
                player->yvel = 8;
                break;
            case SDLK_RIGHT:
                player->xvel = 8;
                break;
            case SDLK_LEFT:
                player->xvel = -8;
                break;
            case SDLK_d:
                if (getCurrentTime() - player->timeWhenShot >= 100 || player->timeWhenShot == 0)
                {
                    playerShoot();
                }
                break;
            case SDLK_s:
                spawnEnemy();
                break;
            default:
                break;
            }
            break;

        case SDL_KEYUP:
            switch (event.key.keysym.sym)
            {
            case SDLK_UP:
                if (player->yvel < 0)
                    player->yvel = 0;
                break;
            case SDLK_DOWN:
                if (player->yvel > 0)
                    player->yvel = 0;
                break;
            case SDLK_RIGHT:
                if (player->xvel > 0)
                    player->xvel = 0;
                break;
            case SDLK_LEFT:
                if (player->xvel < 0)
                    player->xvel = 0;
                break;
            default:
                break;
            }
            break;

        default:
            break;
        }
    }
}

void Game::update()
{
    if (checkPlayerDead() == true && isGameOver == false)
    {
        gameOver();
    }
    else
    {
        scoreBar->text = "Score: " + std::to_string(Game::score);

        getCurrentTime();
        int timeSinceEnemySpawned = 0;
        timeSinceEnemySpawned = currentTime - enemySpawned;

        if (checkPlayerDead() == false && timeSinceEnemySpawned >= 1000)
        {
            spawnEnemy();
            spawnEnemyVertical();
            timeSinceEnemySpawned = 0;
        }
        isPlayerInvulnerable();
        checkCollisions();

        //update every object, remove if tagged
        for (auto &object : objects)
        {
            if (object->notRemovable == false)
            {
                objects.remove(object);
                delete object;
                break;
            }
            else
            {
                object->update();
            }
        }
    }
}

void Game::render()
{
    SDL_RenderClear(renderer);
    for (auto &object : objects)
    {
        object->render();
    }

    SDL_RenderPresent(renderer);
}

void Game::clean()
{
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);
    TTF_CloseFont(gFont);
    TTF_Quit();
    SDL_Quit();
    std::cout << "Game cleaned\n";
}

void Game::isPlayerInvulnerable()
{
    //determine if player is invulnerable after being hit
    if (!(player->timeWhenHit < 0))
    {
        player->timeSinceHit = currentTime - player->timeWhenHit;
    }

    if (player->timeSinceHit >= 2000)
    {
        player->isVulnerable = true;
    }
}

void Game::checkCollisions()
{
    //check enemy collisions with player
    for (auto &enemy : objects)
    {
        if (!(enemy->notEnemy == true))
        {
            if (SDL_HasIntersection(&player->collisionRect, &enemy->collisionRect))
            {
                if (player->isVulnerable)
                {
                    player->isVulnerable = false;
                    player->lowerHealth();
                    player->xpos = 80;
                    player->ypos = 280;
                    player->timeWhenHit = SDL_GetTicks();

                    if (player->health == 2)
                    {
                        objects.remove(heart3);
                    }
                    else if (player->health == 1)
                    {
                        objects.remove(heart2);
                    }
                    else if (player->health == 0)
                    {
                        objects.remove(heart1);
                    }

                    if (player->health == 0)
                    {
                        //isRunning = false;
                        player->dead = true;
                        objects.remove(player);
                    }
                }
            }

            for (auto &bullet : objects)
            {
                if (bullet->nonProjectile == false)
                {

                    if (SDL_HasIntersection(&bullet->collisionRect, &enemy->collisionRect))
                    {
                        enemy->notRemovable = false;
                        Game::increaseScore(100);
                        bullet->notRemovable = false;
                        break;
                    }
                }
            }
        }
    }
}

int Game::getCurrentTime()
{
    //get time elapsed since game was launched, required for timing
    currentTime = SDL_GetTicks();
    return currentTime;
}

bool Game::checkPlayerDead()
{
    return player->dead;
}

void Game::playerShoot()
{
    Bullet *bullet1 = new Bullet(playerBulletBasicImage, player->xpos + 33, player->ypos, 64, 32);
    //Bullet *bullet2 = new Bullet(playerBulletBasicImage, player->xpos + 103, player->ypos, 64, 32);
    Bullet *bullet3 = new Bullet(playerBulletBasicImage, player->xpos + 173, player->ypos, 64, 32);
    objects.emplace_back(bullet1);
    //objects.emplace_back(bullet2);
    objects.emplace_back(bullet3);
    player->timeWhenShot = getCurrentTime();
}

void Game::spawnEnemy()
{
    enemySpawned = SDL_GetTicks();
    int randomy = rand() % 500 + 50;
    //  int randomx = rand() % 800 + 700;
    Enemy *enemy = new Enemy(enemyImage, 832, randomy, 32, 32);
    enemy->xvel = -8;
    enemy->yvel = 1;
    objects.emplace_back(enemy);
}

void Game::spawnEnemyVertical()
{
    enemySpawned = SDL_GetTicks();
    int randomy = rand() % 500 + 50;
    int randomx = rand() % 800 + 400;
    Enemy *enemy = new Enemy(enemyImage, SCREENHEIGHT + 32, randomy, 32, 32);
    enemy->xvel = -5;
    enemy->yvel = -2;
    objects.emplace_back(enemy);
}

void Game::increaseScore(int bonus)
{
    score += bonus;
}

void Game::gameOver()
{
    isGameOver = true;
    gameOverScreen->text = "GAME OVER";
   // gameOverScreen->sourceRect.w = 320;
    objects.emplace_back(gameOverScreen);
}
