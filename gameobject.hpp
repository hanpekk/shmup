#ifndef GAMEOBJECT_HPP
#define GAMEOBJECT_HPP
#include "game.hpp"

class GameObject
{
public:
    GameObject(const char *texturesheet, int x, int y, int xvel, int yvel, int sizew, int sizeh);
    GameObject(const char *texturesheet, int x, int y, int sizew, int sizeh);
    GameObject();
    ~GameObject();

    virtual void update();
    virtual void render();

    int xpos=0;
    int ypos=0;

    int xvel=0;
    int yvel=0;

    bool nonProjectile=true;
    bool notRemovable=true;
    bool notEnemy=true;

    SDL_Texture *objTexture;
    SDL_Rect sourceRect, destinationRect, collisionRect;

private:
};

class Background : public GameObject
{
public:
    Background(const char *texturesheet, int x, int y, int sizew, int sizeh, int xvelocity);
    void update();
};

class Bullet : public GameObject
{
    public:
    Bullet(const char *texturesheet, int x, int y, int sizew, int sizeh);
    void update();
};

class GameText : public GameObject
{
    public: 
    GameText(std::string textureText, int x, int y);
    GameText(int x, int y);
    void update();
    void render();
    
    std::string text;
};

#endif