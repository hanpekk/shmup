#ifndef ENEMY_HPP
#define ENEMY_HPP

#include "gameobject.hpp"

class Enemy : public GameObject
{
public:
    Enemy(const char *texturesheet, int x, int y, int sizew, int sizeh);
    Enemy();
    ~Enemy();

    void update();
    void render();

    SDL_Texture *objTexture;
    SDL_Rect sourceRect, destinationRect, collisionArea;

};

#endif