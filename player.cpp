#include "gameobject.hpp"
#include "player.hpp"
#include "texturemanager.hpp"
#include "constants.hpp"
#include <iostream>



Player::Player(const char *texturesheet, int x, int y)
{
    objTexture = TextureManager::LoadTexture(texturesheet);
    timeWhenHit = 0;
    timeWhenShot = 0;
    timeSinceHit = 0;

    xpos = x;
    ypos = y;
    health = 3;
}

Player::Player() {}
Player::~Player() {}

void Player::update()
{

    if (!(xvel == 0))
    {
        xpos += xvel;
    }

    if (!(yvel == 0))
    {
        ypos += yvel;
    }

    sourceRect.h = 32;
    sourceRect.w = 32;
    sourceRect.x = 0;
    sourceRect.y = 0;

    destinationRect.x = xpos;
    destinationRect.y = ypos;
    destinationRect.w = sourceRect.w;
    destinationRect.h = sourceRect.h;

    collisionRect.x = xpos;
    collisionRect.y = ypos;
    collisionRect.w = 32;
    collisionRect.h = 32;

    if (xpos >= SCREENWIDTH - STATUSBARHEIGHT)
    {
        xpos = SCREENWIDTH - STATUSBARHEIGHT;
    }
    if (xpos <= 10)
    {
        xpos = 10;
    }
    if (ypos >= SCREENHEIGHT - STATUSBARHEIGHT)
    {
        ypos = SCREENHEIGHT - STATUSBARHEIGHT;
    }
    if (ypos <= STATUSBARHEIGHT)
    {
        ypos = STATUSBARHEIGHT;
    }
}

void Player::render()
{
    SDL_RenderCopy(Game::renderer, objTexture, &sourceRect, &destinationRect);
}

void Player::lowerHealth()
{
    health--;
}

