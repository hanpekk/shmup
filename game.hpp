#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>
#include <SDL2/SDL_ttf.h>
#include <stdio.h>
#include <string>

#ifndef GAME_HPP
#define GAME_HPP
class Game
{
public:
Game();
    ~Game();
 
    void init(const char *title, int xpos, int ypos, int width, int heigth, bool fullscreen);

    void handleEvents();
    void update();
    void render();
    void clean();

    void isPlayerInvulnerable();
    void checkCollisions();
    bool checkPlayerDead();
    int getCurrentTime();
    int enemySpawned = 0;
    void playerShoot();
    void spawnEnemy();
    void spawnEnemyVertical();

    bool isGameOver = false;

    Uint32 currentTime;
    bool running() { return isRunning; }

    static int score;
    static void increaseScore(int bonus);

    static SDL_Renderer *renderer;
    static TTF_Font *gFont;

    void gameOver();

private:
    int count = 0;
    bool isRunning;
    SDL_Window *window;
};

#endif /* GAME_HPP */